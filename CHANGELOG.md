##[0.0.9](https://gitlab.com/innotecuk-public/innobuggy/commit/ef56c4a13069337dec3a0d7c493192313ca74c6a) - 28-6-2-18

Performed straight line and in place rotation tests on the physical robot. Results show that for straight lines encoders
are relatively accurated and the IMU on its own can measure in place rotation reliably. The EKF node basically relies
on the odom topic for translation and the IMU topic for orientation. Time for ```robot_localization``` tests

### Added
   - [hardware_interface_differential_drive](hardware_interface_differential_drive/scripts)Test Files:
   
      - [distance_from_encoders.py](hardware_interface_differential_drive/scripts/distance_from_encoders.py): Prints out the
      distance travelled in a straight line as measured by the internal odometry publisher and the ekf node
      
      - [angle_from_encoders_imu.py](hardware_interface_differential_drive/scripts/angle_from_encoders_imu.py): Prints out the
            angle rotated in place as measured by the internal odometry publisher and the ekf node and the IMU node
### Changed

- Simulation package: Added a differential drive, skid steering and laser scanner plugins to the gazebo model. However gazebo
segfaults when anything touches the scanner beams. Seems to be an issue with the laser plugin implementation.
    
##[0.0.8](https://gitlab.com/innotecuk-public/innobuggy/commit/5bf7750925b5690dfe9642343fe9df83ac58c249) - 26-6-2-18
### Added
   - [odometry](hardware_interface_differential_drive/src/odometry.cpp) now keeps  ```ticks_per_rotation``` and ```ticks_per_degree``` for a full wheel rotation and a degree of tank
   steering respectively
   - Getters and setters for the private variables
   - Two new methods for testing steering accuracy in ```odometry.cpp```. ```onPointTurn(degrees)``` rotates the robot in place according to the input degrees and ```ticks_per_degree```.
   ```straightLine(cm)``` sends the robot forwards or backwards according to the input and ```ticks_per_rotation``` attributes.
   - A new launch file that launches a laser scan matcher node for odometry through the Hokuyo scanner. Run
   ```bash
   inno@inno:~$ roslaunch ekf_launcher laser_scan_matcher.launch 
   ```
   - Reactivated the Hokuyo plugin in the Gazebo simulation files
### Changed
- [odometry](hardware_interface_differential_drive/src/odometry.cpp), 
[hardware_interface_differential_drive](hardware_interface_differential_drive/src/hardware_interface.cpp) and
 [manual_control](hardware_interface_differential_drive/src/manual_control.cpp) had their mains moved to separate files,
 and can be accessed as nodes the same way as before. The only difference is now the ```*_spin.cpp``` files are used in
 CMakeLists.txt
   

0.0.7 phidgets IMU package added to launch properly the connected IMU

0.0.6 old innoclimber_base package deleted

0.0.5 ekf_launcher package added

0.0.4 Gradual transition to the new hardware interface

0.0.3 Common parameters file added
In innoclimber_base, we added a commmon_parameters.yaml to host the common parameters for all nodes such as rate. All launch files changed accordingly.

0.0.2 Update to project elements
Readme and CHANGELOG added to the project
