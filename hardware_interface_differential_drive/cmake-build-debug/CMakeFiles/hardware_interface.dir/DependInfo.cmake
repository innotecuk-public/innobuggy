# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/lydakis/innobuggy/hardware_interface_differential_drive/src/wiringSerial.c" "/home/lydakis/innobuggy/hardware_interface_differential_drive/cmake-build-debug/CMakeFiles/hardware_interface.dir/src/wiringSerial.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"hardware_interface_differential_drive\""
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/lydakis/innobuggy/hardware_interface_differential_drive/src/arduinoSerial.cpp" "/home/lydakis/innobuggy/hardware_interface_differential_drive/cmake-build-debug/CMakeFiles/hardware_interface.dir/src/arduinoSerial.cpp.o"
  "/home/lydakis/innobuggy/hardware_interface_differential_drive/src/hardware_interface.cpp" "/home/lydakis/innobuggy/hardware_interface_differential_drive/cmake-build-debug/CMakeFiles/hardware_interface.dir/src/hardware_interface.cpp.o"
  "/home/lydakis/innobuggy/hardware_interface_differential_drive/src/hardware_interface_spin.cpp" "/home/lydakis/innobuggy/hardware_interface_differential_drive/cmake-build-debug/CMakeFiles/hardware_interface.dir/src/hardware_interface_spin.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_PACKAGE_NAME=\"hardware_interface_differential_drive\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
