cmake_minimum_required(VERSION 2.8.3)
project(hardware_interface_differential_drive)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11 -Wno-narrowing)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  tf
#  std_msgs
#  message_generation
)

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)


## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

## Generate messages in the 'msg' folder
#add_message_files(
#   FILES
#   Encoders.msg
#)

## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
#generate_messages(
#   DEPENDENCIES
#   std_msgs
#)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES hardware_interface_arm
#  CATKIN_DEPENDS roscpp
#  DEPENDS system_lib
)

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
# add_library(${PROJECT_NAME}
#   src/${PROJECT_NAME}/hardware_interface_arm.cpp
# )

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
# add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Declare a C++ executable
## With catkin_make all packages are built within a single CMake context
## The recommended prefix ensures that target names across packages don't collide
#add_executable(main src/main.cpp src/add.cpp)
add_executable(stand_alone_spin src/stand_alone_spin.cpp src/arduinoSerial.cpp src/wiringSerial.c)
add_executable(hardware_interface src/hardware_interface_spin.cpp src/hardware_interface.cpp src/arduinoSerial.cpp src/wiringSerial.c)
add_executable(manual_control src/manual_control_spin.cpp src/manual_control.cpp)
add_executable(odometry src/odometry_spin.cpp)
add_executable(odometry_test src/odometry_test.cpp)

## Rename C++ executable without prefix
## The above recommended prefix causes long target names, the following renames the
## target back to the shorter version for ease of user use
## e.g. "rosrun someones_pkg node" instead of "rosrun someones_pkg someones_pkg_node"
# set_target_properties(${PROJECT_NAME}_node PROPERTIES OUTPUT_NAME node PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(${PROJECT_NAME}_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
# add_dependencies(hardware_interface hardware_interface_differential_drive_generate_messages_cpp)

## Specify libraries to link a library or executable target against
target_link_libraries(hardware_interface ${catkin_LIBRARIES})
target_link_libraries(manual_control ${catkin_LIBRARIES})
target_link_libraries(odometry ${catkin_LIBRARIES})
target_link_libraries(odometry_test ${catkin_LIBRARIES})
