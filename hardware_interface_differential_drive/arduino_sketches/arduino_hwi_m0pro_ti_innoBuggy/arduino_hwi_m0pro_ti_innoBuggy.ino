#include "config.h"
#include "avdweb_SAMDtimer.h"

// struct of the outgoing package inlcuding encoders
typedef struct {
  char startMark;
  float encoder1;
  float encoder2;
  byte checksum;
  char endMark;
} __attribute__((__packed__))data_packet_t; 

data_packet_t wheels;

const int baud_rate = SERIAL_BAUD_RATE;
const byte numBytes = NUM_OF_INCOMING_BYTES;
const byte numPayloadBytes = numBytes - 2;
byte receivedBytes[numBytes];
byte polarity;
byte v1, v2;
int v1_sign, v2_sign;

boolean newData = false;

// ---- LEFT side ----   
int pwm_left = 5;
int dir1_left = 6;
int dir2_left = 7;

int encoder1A=12;
int encoder1B=13;

volatile float encoder1=0;
volatile int past_en1=0;

// ---- RIGHT side ----
int pwm_right = 3;
int dir1_right = 2;
int dir2_right = 4;

int encoder2A=0;
int encoder2B=1;

volatile float encoder2=0;
volatile int past_en2=0;

volatile int del=5;//delay step in the encoder interrupt (low-pass filter)

// fixed speed for the manual tests
byte speed = 60;

// encoders
int resetEncodersButton = A0;
int resetEncodesValue = 0;

//buttons:
int mode=A2;
int set_dir=A3;
int mot1=A4;
int mot2=A5;


void timer4_interrupt(struct tc_module *const module_inst) 
{ 
  /*
  unsigned long StartTime = millis();
  Serial.print(StartTime);
  Serial.println(" hey");
  */
  // ---- Send encoders to rPi (ROS hardware interface)
  sendEncoders();
   
  // ---- Receive the motor velocities from rPi (ROS hardware interface) 
  receiveMotorVelocities();
    
  // ---- Log input values (ONLY)
  //newDataAvailable();
  
  // ---- Rotate motors
  rotateMotors();
}

SAMDtimer timer4 = SAMDtimer(4, timer4_interrupt, 20000);

void setup()                    
{
  // begin serial in programming USB serial for debugging
  Serial.begin(baud_rate);
  // begin serial in native USB for connection with rPi3
  SerialUSB.begin(baud_rate);           

  // demo values for encoders, we assume that are floats
  wheels.startMark = '<';
  wheels.encoder1 = 0;
  wheels.encoder2 = 0;
  wheels.endMark = '>';

  // ---- Set the mode for motors---- 
  pinMode(pwm_left, OUTPUT);
  pinMode(dir1_left, OUTPUT);
  pinMode(dir2_left, OUTPUT);
  
  pinMode(pwm_right, OUTPUT);
  pinMode(dir1_right, OUTPUT);
  pinMode(dir2_right, OUTPUT);

  // ---- Set the mode for encoder ----
  pinMode(encoder1A,INPUT);
  pinMode(encoder1B,INPUT);
  
  pinMode(encoder2A,INPUT);
  pinMode(encoder2B,INPUT);

  // ---- encoder pull-up and interrupt ---- 
  digitalWrite(encoder1A,HIGH);
  digitalWrite(encoder1B,HIGH);
  attachInterrupt(encoder1A,enc1,CHANGE);
  
  digitalWrite(encoder2A,HIGH);
  digitalWrite(encoder2B,HIGH);
  attachInterrupt(encoder2A,enc2,CHANGE);

  // ---- set the buttons mode ----
  pinMode(mode, INPUT);
  pinMode(set_dir, INPUT);
  pinMode(mot1, INPUT);
  pinMode(mot2, INPUT);
  pinMode(resetEncodersButton, INPUT);
  
  // ---- pullup resistors on buttons ----
  digitalWrite(mode,HIGH);
  digitalWrite(set_dir, HIGH);
  digitalWrite(mot1, HIGH);
  digitalWrite(mot2, HIGH);
  digitalWrite(resetEncodersButton, HIGH);

}

void loop()                       
{
  if (!digitalRead(mode)) {
    // ---- Maunual button mode ----
    if(!digitalRead(set_dir)){
      if(!digitalRead(mot1)){
        analogWrite(pwm_left, speed);
        digitalWrite(dir1_left, HIGH);
        digitalWrite(dir2_left, LOW);
      } else {
        analogWrite(pwm_left, 0);
      }
     
      if(!digitalRead(mot2)){
        analogWrite(pwm_right, speed);
        digitalWrite(dir1_right, HIGH);
        digitalWrite(dir2_right, LOW);
      } else {
        analogWrite(pwm_right, 0);
      }
       
    } else {
      //another direction of rotation
      
      if(!digitalRead(mot1)){
        analogWrite(pwm_left, speed);
        digitalWrite(dir1_left, LOW);
        digitalWrite(dir2_left, HIGH);
      } else {
        analogWrite(pwm_left, 0);
      }
      
      if(!digitalRead(mot2)){
        analogWrite(pwm_right, speed);
        digitalWrite(dir1_right, LOW);
        digitalWrite(dir2_right, HIGH);
      } else {
        analogWrite(pwm_right, 0);
      }
    }   
  } else {
    
    // ---- ROS controls the motors ----
    if(!digitalRead(resetEncodersButton)) {
      resetEncoders();
    }
    
    //sendEncoders();
    
    // ---- Insert delay for bi-directional communication
    delay(50);
  
    // ---- Receive the motor velocities from rPi (ROS hardware interface) 
    //receiveMotorVelocities();
    
    // ---- Log only the input values ----
    //showNewData();
    
    // ---- rotate the wheels ----
    //rotateMotors();
    
    // ---- Test motors manually ----
    
    // Left
    //analogWrite(pwm_left, speed);
    //digitalWrite(dir1_left, LOW);
    //digitalWrite(dir2_left, HIGH);
  
    // Right
    //analogWrite(pwm_right, speed);
    //digitalWrite(dir1_right, LOW);
    //digitalWrite(dir2_right, HIGH);
  }
}

//-----------------------------------------------------------
//

bool calc_incoming_checksum() 
{
  byte check_sum;
  int sum = 0;
  for (int i = 0; i < numPayloadBytes-1; i++) {
    
    sum += receivedBytes[i];
  }

  check_sum = 255 - (sum%255);
 
  //SerialUSB.println(check_sum, HEX);

  if (check_sum == receivedBytes[numPayloadBytes - 1]) {
    return true;
  } else {
    return false;
  }
}

//-----------------------------------------------------------
//

byte calc_outgoing_checksum() 
{ 
  unsigned long uBufSize = sizeof(data_packet_t);
  byte pBuffer[uBufSize];
  int sum = 0;
  byte check_sum;
  
  //SerialUSB.print("--> ");
  //SerialUSB.println(uBufSize);
  
  memcpy(pBuffer, &wheels, uBufSize);
  for(int i = 1; i<uBufSize-2;i ++) {
    //SerialUSB.println(pBuffer[i], HEX);
    sum += pBuffer[i];
  }
  //SerialUSB.println(sum, HEX);

  check_sum = 255 - sum%255;
  //SerialUSB.print("--> ");
  //SerialUSB.println(check_sum, HEX);

  return check_sum;
}

//-----------------------------------------------------------
//
void resetEncoders()
{
  encoder1 = 0;
  encoder2 = 0;
}


//-----------------------------------------------------------
//

void sendEncoders()
{
  noInterrupts(); //disable all interrupts

  wheels.startMark = '<';
  wheels.encoder1 = encoder1;
  wheels.encoder2 = encoder2;
  wheels.endMark = '>';
  
  interrupts(); //enable all interrupts
  
  // calculate the check_sum of the outgoing frame and send it 
  wheels.checksum = calc_outgoing_checksum();
  
  unsigned long uBufSize = sizeof(data_packet_t);
  char pBuffer[uBufSize];
  
  memcpy(pBuffer, &wheels, uBufSize);
  for(int i = 0; i<uBufSize;i ++) {
   SerialUSB.print(pBuffer[i]);
  }
  SerialUSB.flush();
  logOutput();
}

//-----------------------------------------------------------
//

void receiveMotorVelocities() 
{
  static boolean recvInProgress = false;
  static byte ndx = 0;
  byte startMarker = 0x3C;
  byte endMarker = 0x3E;
  byte rc;

  //SerialUSB.println("Debug: Intro");

  while (SerialUSB.available() > 0 && newData == false) {
    rc = SerialUSB.read();

    //SerialUSB.println("Debug: Receive bytes");
    
    if (recvInProgress == true) {

      //SerialUSB.println("Debug: Receive in progress");
      
      if (rc != endMarker) {

        //SerialUSB.println("Debug: Fill-in the array");
        
        receivedBytes[ndx] = rc;
        ndx++;
        if (ndx >= numBytes) {
          ndx = numBytes - 1;
        } 
      } else {
        //SerialUSB.println("Debug: Filled array");
        recvInProgress = false;
        ndx = 0;
        newData = true;
      }
    } else if (rc == startMarker) {
      recvInProgress = true;
    } 
  }
}

//-----------------------------------------------------------
//

void newDataAvailable() {
  if (newData == true) {
    if (calc_incoming_checksum()) {
      
      //SerialUSB.println("Debug: Valid data received");

      //SerialUSB.println("Debug: Received ");

      polarity = receivedBytes[0];
      //Serial.println(receivedBytes[0], BIN);
      
    
      v1_sign = getPolarity(polarity, 1);
      v1 = receivedBytes[1];
      //SerialUSB.println(v1_sign);
    
      v2_sign = getPolarity(polarity, 2);
      v2 = receivedBytes[2];
      //SerialUSB.println(v2_sign);
    
      Serial.print("v1 = ");
      Serial.print(v1_sign*(int)v1);
      Serial.print(" v2 = ");
      Serial.println(v2_sign*(int)v2);
      
    } else {
      Serial.println("Debug: NOT valid data received");
    }

    //SerialUSB.println();
    newData = false;
  }
}

//-----------------------------------------------------------
//

void logOutput()
{ 
  Serial.print("Outgoing data: ");
  Serial.print(wheels.encoder1);
  Serial.print(" - ");
  Serial.print(wheels.encoder2);
  Serial.print(" - ");
  Serial.println(wheels.checksum, HEX);
}

//-----------------------------------------------------------
//

int getPolarity(byte input, int index)
{
  if(bitRead(input, index-1)) {
    return -1; 
  } else {
    return 1;
  }
}

//-----------------------------------------------------------
//

void rotateMotors() {
  if (newData == true) {
    if (calc_incoming_checksum()) {
      
      //SerialUSB.println("Debug: Valid data received");
      //SerialUSB.println("Debug: Received ");

      polarity = receivedBytes[0];
      //SerialUSB.println(receivedBytes[0], BIN);
    
      v1_sign = getPolarity(polarity, 1);
      v1 = receivedBytes[1];
      //Serial.println(v1_sign);
    
      v2_sign = getPolarity(polarity, 2);
      v2 = receivedBytes[2];
      //Serial.println(v2_sign);
    
      
      analogWrite(pwm_left, (int)v1);
      if (v1_sign >= 0) {
        
        digitalWrite(dir1_left, HIGH);
        digitalWrite(dir2_left, LOW);
      } else {
        
        digitalWrite(dir1_left, LOW);
        digitalWrite(dir2_left, HIGH);
      }

      analogWrite(pwm_right, (int)v2);
      if (v2_sign >= 0) {
        
        digitalWrite(dir1_right, HIGH);
        digitalWrite(dir2_right, LOW);
      } else {
        
        digitalWrite(dir1_right, LOW);
        digitalWrite(dir2_right, HIGH);
      }
            
      
      Serial.print("v1 = ");
      Serial.print(v1_sign*(int)v1);
      Serial.print(" v2 = ");
      Serial.println(v2_sign*(int)v2);
    } else {
      Serial.println("Debug: NOT valid data received");
    }

    //SerialUSB.println();
    newData = false;
  }
}

//-----------------------------------------------------------
//

//interrupt of the 1st encoder:
void enc1(){
  
  //debounce:
  for(int k=0;k<del;k++){
  }//nop - little delay
  
  if(digitalRead(encoder1A)!=past_en1){ //if the state different?
      
      //once again:
      for(int k=0;k<del;k++){
      }//nop - little delay
      
      if(digitalRead(encoder1A)!=past_en1) //check once again
      {
          //increase the counting:
          if(digitalRead(encoder1A)==digitalRead(encoder1B))
          {
            encoder1++;
          }else
          {
            encoder1--;
          }
          
          past_en1=!past_en1;
      }
  }
}

//interrupt of the 2nd encoder:
void enc2(){
  
  //debounce:
  for(int k=0;k<del;k++){
  }//nop - little delay
  
  if(digitalRead(encoder2A)!=past_en2){//if the state different?
      
      //once again:
      for(int k=0;k<del;k++){
      }//nop - little delay
      
      if(digitalRead(encoder2A)!=past_en2)//check once again
      {
          //increase the counting:
          if(digitalRead(encoder2A)==digitalRead(encoder2B))
          {
            encoder2--;
          }else
          {
            encoder2++;
          }
          past_en2=!past_en2;
      }
  }
}


