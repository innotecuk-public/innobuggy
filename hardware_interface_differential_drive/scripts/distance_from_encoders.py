#!/usr/bin/env python

import rospy

import math

from sensor_msgs.msg import Joy
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseWithCovarianceStamped
from nav_msgs.msg import Odometry


def calculate_distance(point_a, point_b):
    dist = [(a - b) ** 2 for a, b in zip(point_a, point_b)]
    dist = math.sqrt(sum(dist))
    return dist


class CompareDistances:
    def joy_callback(self, joy_msg):
        if joy_msg.buttons[1] == 1:
            self.new_enc = True
            self.new_imu = True
            self.new_ekf = True
            rospy.logerr("Resetting Poses")
            pass

    def ekf_callback(self, ekf):
        if self.new_ekf:
            self.new_ekf = False
            self.first_ekf_pose = ekf.pose.pose
        self.current_ekf_pose = ekf.pose.pose

    def odom_callback(self, encoders_msg):
        if self.new_enc:
            self.new_enc = False
            self.first_encoders = encoders_msg.pose.pose
        self.current_encoders = encoders_msg.pose.pose

    def __init__(self):
        self.first_imu = []
        self.first_ekf_pose = Pose()
        self.first_encoders = Pose()
        self.current_imu = []
        self.current_encoders = Pose()
        self.current_ekf_pose = Pose()

        self.new_imu = True
        self.new_enc = True
        self.new_ekf = True

        rospy.Subscriber("joy", Joy, self.joy_callback, queue_size=10)
        rospy.Subscriber("odom", Odometry, self.odom_callback, queue_size=10)
        rospy.Subscriber("robot_pose_ekf/odom_combined", PoseWithCovarianceStamped, self.ekf_callback, queue_size=10)
        rospy.init_node('compare_angles_node')
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            # if self.current_imu:
            #     rospy.loginfo("According to the imu the robot has moved " + str(
            #         calculate_distance(self.first_imu, self.current_imu)) + " cm.")
            if self.current_encoders:
                rospy.loginfo("According to the encoders the robot has moved " + str(calculate_distance(
                    [self.first_encoders.position.x, self.first_encoders.position.y],
                    [self.current_encoders.position.x, self.current_encoders.position.y])) + " m.")

            if self.current_ekf_pose:
                rospy.loginfo("According to the EKF the robot has moved " + str(calculate_distance(
                    [self.first_ekf_pose.position.x, self.first_ekf_pose.position.y],
                    [self.current_ekf_pose.position.x, self.current_ekf_pose.position.y])) + " m.")
            r.sleep()


if __name__ == "__main__":
    CompareDistances()