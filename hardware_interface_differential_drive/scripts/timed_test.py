#!/usr/bin/env python

import rospy

from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist


class TimedTest:

    def odom_callback(self, data):
        self.odom = data

        if self.done and self.started and data.twist.twist.linear.x == 0.0 and data.twist.twist.linear.z == 0.0:
            self.end_time = rospy.Time.now()
            self.done = True
        pass

    def __init__(self, duration=0.5, linx=0.0, angz=-0.3):
        rospy.init_node("timed_test")
        self.start_time = rospy.Time.now()
        self.end_time = []
        self.last_pub = rospy.Time.now()
        r = rospy.Rate(100)
        self.odom = Odometry()
        # go message
        self.velocities = Twist()
        self.velocities.linear.x = linx
        self.velocities.angular.z = angz
        # stop message
        self.stop = Twist()
        self.stop.linear.x = 0.0
        self.stop.angular.z = 0.0

        self.started = False
        self.done = False
        self.pub = rospy.Publisher("cmd_vel", Twist, queue_size=1)
        self.sub = rospy.Subscriber("odom", Odometry, self.odom_callback)

        while not rospy.is_shutdown():

            if self.end_time:
                break

            if (rospy.Time.now() - self.start_time).to_sec() <= duration:
                if not self.started:
                    rospy.loginfo("Publishing first message at: " + str(rospy.Time.now()))
                    self.started = True
                self.pub.publish(self.velocities)
            else:
                self.last_pub = rospy.Time.now()
                self.pub.publish(self.stop)
                self.done = True
            r.sleep()

        rospy.loginfo("Delay between last message published and odometry update: " + str(
            (self.end_time - self.last_pub).to_sec()) + " s")
        rospy.loginfo("Transaction took " + str((self.end_time - self.start_time).to_sec()) + "s")


if __name__ == "__main__":
    TimedTest()
