//
// Created by lydakis on 25/06/18.
//
#include "odometry.cpp"

int main(int argc, char **argv) {

    ros::init(argc, argv, "odometry_node");

    ROS_INFO("Starting the odometry node\n");
    differential_odometry odometry_feedback;

    odometry_feedback.spin();

    return 0;
}
