#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>

#include "wiringSerial.h"

class Arduino {

public:
  unsigned long int count;

  Arduino(const char *, const int);
  void send_to_mc(int16_t *);
  int read_from_mc(float *);
  void close_serial();

private:
  int fd;
  union {
    float f;
    unsigned char b[4];
  } u, v;
  unsigned char inputBuffer[10];
  unsigned char outputBuffer[6];

  void config_velocityPolarity(unsigned char *, int16_t *);
  unsigned char calc_outgoing_checksum(unsigned char *);
  int calc_incoming_checksum(unsigned char *);
};
