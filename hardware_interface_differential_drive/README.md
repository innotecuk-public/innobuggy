# Hardware inteface differential drive

This package works with Arduino M0 pro to send PWMs values for left and right side motors, get encoders and calculate odometry data.

This library uses wiringSerial library ported also to non raspeberry pis boards (e.g. laptops).

In the Arduino parameters config files we can setup the baud rate and the time interval between 
reception and transmission.


## Execution

- To bring up hardware interface node:

```bash
inno@inno:~$ roslaunch hardware_interface_differential_drive hardware_interface.launch
```

- To test hardcoded command values please use the following script:

```bash
inno@inno:~$ rosrun hardware_interface_differential_drive test_motion_cmd.launch
```

### Compatibility

Software
- Ubuntu Mate 16.04
- ROS Kinetic
- Arduino IDE 1.8.x


Hardware
- Raspberry Pi3, VirtualBox Ubuntu virtual machine 
- Arduino M0 pro


### Project Members

- Angelos Plastropoulos
