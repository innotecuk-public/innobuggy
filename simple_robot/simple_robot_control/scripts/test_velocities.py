#!/usr/bin/env python

import rospy
import sys

from std_msgs.msg import Float32MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension


def test_velocoties(numOfWheels):
    pub = rospy.Publisher('joint_velocities', Float32MultiArray, queue_size=5)

    rospy.init_node('joint_velocities', anonymous=True)
    rate = rospy.Rate(20)

    # compose the multiarray message
    joint_velocities = Float32MultiArray()
    my_layout = MultiArrayLayout()
    my_multiarray_dimension = MultiArrayDimension()

    my_multiarray_dimension.label = "joint_velocities"
    my_multiarray_dimension.size = 1
    my_multiarray_dimension.stride = numOfWheels

    my_layout.dim = [my_multiarray_dimension]
    my_layout.data_offset = 0
    joint_velocities.layout = my_layout

    while not rospy.is_shutdown():
        # first item is left and second is right
        if numOfWheels == 2:
            joint_velocities.data = [1.0, 1.0]
        elif numOfWheels == 4:
            joint_velocities.data = [1.0, 1.0, 1.0, 1.0]
        pub.publish(joint_velocities)
        rate.sleep()


if __name__ == '__main__':
    numOfWheels = int(sys.argv[1])
    try:
        test_velocoties(numOfWheels)
    except rospy.ROSInterruptException:
        pass
