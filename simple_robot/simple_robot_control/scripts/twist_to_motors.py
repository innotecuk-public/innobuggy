#!/usr/bin/env python

import rospy
import sys

from geometry_msgs.msg import Twist

from std_msgs.msg import Float32MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension


class TwistToMotors:

    def __init__(self, num_of_wheels):
        self.linearVelocity = 0.0
        self.angularVelocity = 0.0
        self.right = 0.0
        self.left = 0.0
        self.last_twist_time = 0.0
        self.lin_mul = 1.0
        self.ang_mul = 1.0
        self.wheel_radius = 0.0
        # number of wheels
        self.numOfWheels = num_of_wheels
        # Create the publishers
        self.joint_velocities_Pub = rospy.Publisher('joint_velocities', Float32MultiArray, queue_size=10)

        # Load the configuration file
        self.base_width = 0.0
        self.rate = 0.0
        self.timeout = 0.0
        self.node_name = ""

    def publish(self):
        # compose the multiarray message
        joint_velocities = Float32MultiArray()
        layout = MultiArrayLayout()
        multi_array_dimension = MultiArrayDimension()

        multi_array_dimension.label = "joint_velocities"
        multi_array_dimension.size = 1
        multi_array_dimension.stride = self.numOfWheels

        layout.dim = [multi_array_dimension]
        layout.data_offset = 0
        joint_velocities.layout = layout

        if rospy.get_time() - self.last_twist_time < self.timeout:
            #
            # self.right = self.lin_mul * self.linearVelocity + self.ang_mul * self.angularVelocity * self.base_width / 2
            # self.left = self.lin_mul * self.linearVelocity - self.ang_mul * self.angularVelocity * self.base_width / 2

            self.left = (self.linearVelocity - self.angularVelocity * self.base_width / 2.0) / self.wheel_radius
            self.right = (self.linearVelocity + self.angularVelocity * self.base_width / 2.0) / self.wheel_radius

            # self.right = ((self.angularVelocity * self.ang_mul * self.base_width) / 2 + self.linearVelocity)
            # self.left = self.linearVelocity * 2 - self.right

            rospy.loginfo("Sending velocities to wheels... vl:{0}, vr:{1}".format(self.left, self.right))

            if self.numOfWheels == 2:
                # first item is left and second is right
                joint_velocities.data = [self.left, self.right]
            elif self.numOfWheels == 4:
                joint_velocities.data = [self.left, self.right, self.left, self.right]
        else:
            if self.numOfWheels == 2:
                joint_velocities.data = [0.0, 0.0]
                # first item is left and second is right
            elif self.numOfWheels == 4:
                joint_velocities.data = [0.0, 0.0, 0.0, 0.0]

        self.joint_velocities_Pub.publish(joint_velocities)

    def twistCallback(self, twist):
        self.linearVelocity = twist.linear.x
        self.angularVelocity = twist.angular.z
        self.last_twist_time = rospy.get_time()

        # rospy.loginfo("linear: {0}, angular: {1}, time: {2}".format(self.linearVelocity, \
        # self.angularVelocity, self.lastTwistTime))

    def main(self):

        # Create the publishers
        self.joint_velocities_Pub = rospy.Publisher('joint_velocities', Float32MultiArray, queue_size=10)

        # Create the corresponding node
        rospy.init_node('twist_to_motors')
        self.node_name = rospy.get_name()

        rospy.loginfo("{0} started".format(self.node_name))

        # Listen to the classic topic for move commands
        rospy.Subscriber("cmd_vel", Twist, self.twistCallback)

        # Load the configuration file
        self.base_width = float(rospy.get_param('~base_width', 0.4))
        self.wheel_radius = float(rospy.get_param('~base_width', 0.1))
        self.rate = float(rospy.get_param('~rate', 20.0))
        self.timeout = float(rospy.get_param('~timeout', 0.2))

        # Loop now
        rate = rospy.Rate(self.rate)

        self.last_twist_time = rospy.get_time()
        while not rospy.is_shutdown():
            self.publish()
            rate.sleep()


if __name__ == '__main__':
    numOfWheels = int(sys.argv[1])
    try:
        node = TwistToMotors(numOfWheels)
        node.main()
    except rospy.ROSInterruptException:
        pass
